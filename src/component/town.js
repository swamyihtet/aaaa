import React from 'react';
import MaterialTable from 'material-table';

function Town_Table() {
  const { useState } = React;

  const [columns, setColumns] = useState([
    { title: 'Towns ID', field: 'towns_id', align:'center'},
    { title: 'Towns Code', field: 'towns_code' ,align:'center'},
    { title: 'Towns Name', field: 'towns_name',align:'center'},
    { title: 'State Region',field: 'state_region' ,align:'center',lookup: { 1: 'Magway Region', 2: 'Mandalay Region', 3:'Naypyidaw Unip Territory', 4:' Kayah State', 5:'Shan State', 6:'Yangon Region',
    7:'Kachin' , 8:'Kayin State', 9:'Mon State', 10: 'Chin State', 11:'Chin', 12:'Mon', 13:'Rakhine', 14:'Shan', 15:'Nay Pyi Taw'}},
    { title: 'Township',field: 'township' ,align:'center',lookup: { 1: 'Pakokku Township', 2: 'Chanmyatharzi Township', 3:'Pyinmana Township', 4:' Loikaw Township', 5:'Kalaw Township', 6:'Bohtataung Township',
    7:'Myitkyina Township' , 8:'Hpa-an Township', 9:'Ye Township', 10: 'Hakha Township'}},
    { title: 'District',field: 'district' ,align:'center',lookup: { 1: 'Pakokku District', 2: 'Mandalay District', 3:'Dekkhina District', 4:' Loikaw District', 5:'Taunggyi District', 6:'East Yangon District',
     7:'Myitkyina District' , 8:'Hpa-an District', 9:'Mawlamyine  District', 10: 'Hakha District'}},
    { title: 'Remark',field: 'remark' ,align:'center'},
  ]);

  const [data, setData] = useState([
    { towns_id: '1', towns_code: '04201', towns_name: 'Pakokku', state_region: 'Magway Region' ,township: 'Pakokku Township', district: 'Pakokku District', remark:''},
    // { towns_id: '2', towns_code: '05041', towns_name: 'Chanmyathazi', state_region: 'Mandalay Region' ,township: 'Chanmyatharzi Township', district: 'Mandalay District', remark:''},
    // { towns_id: '3', towns_code: '15012', towns_name: 'Pyinmana', state_region: 'Naypyidaw Union Territory' ,township: 'Pyinmana Township', district: 'Dekkhina District', remark:''},
    // { towns_id: '4', towns_code: '09011', towns_name: 'Loikaw', state_region: 'Kayah State' ,township: 'Loikaw Township', district: 'Loikaw District', remark:''},
    // { towns_id: '5', towns_code: '06024', towns_name: 'Minewa', state_region: 'Shan State' ,township: 'Kalaw Township', district: 'Taunggyi District', remark:''},
    // { towns_id: '6', towns_code: '11162', towns_name: 'Botahtaung', state_region: 'Yangon Region' ,township: 'Bohtataung Township', district: 'East Yangon District', remark:''},
    // { towns_id: '7', towns_code: '01011', towns_name: 'Myintkyina', state_region: 'Kachin State' ,township: 'Myitkyina Township', district: 'Myitkyina District', remark:''},
    // { towns_id: '8', towns_code: '13018', towns_name: 'Barkat', state_region: 'Kayin State' ,township: 'Hpa-an Township', district: 'Pha-an District', remark:''},
    // { towns_id: '9', towns_code: '12101', towns_name: 'Ye', state_region: 'Mon State' ,township: 'Ye Township', district: 'Mawlamyine  District', remark:''},
    // { towns_id: '10', towns_code: '03011', towns_name: 'Hakha', state_region: 'Chin State' ,township: 'Hakha Township', district: 'Hakha District', remark:''},
  
  ]);
  return (
    <MaterialTable
      title="Town Preview"
      fontFamily="Poppins"
      columns={columns}
      data={data}

      options={{
        paging:false,
        headerStyle:{fontWeight:'bold', fontSize:16, color:'#000', fontFamily:'Poppins'},
        rowStyle:{fontWeight:'normal', fontSize:16, color:'#505050', fontFamily:'Poppins'},
        actionsCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        filterCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        
      }}
      
      editable={{

        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              setData([...data, newData]);
              resolve();
            }, 1000)
          }),

        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataUpdate = [...data];
              const index = oldData.tableData.id;
              dataUpdate[index] = newData;
              setData([...dataUpdate]);

              resolve();
            }, 1000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataDelete = [...data];
              const index = oldData.tableData.id;
              dataDelete.splice(index, 1);
              setData([...dataDelete]);
              
              resolve()
            }, 1000)
          }),
      }}
    />
  )
}

export default Town_Table;