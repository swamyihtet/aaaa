import React from 'react';
import MaterialTable from 'material-table';

function TownShip_Table() {
  const { useState } = React;

  const [columns, setColumns] = useState([
    { title: 'Township ID', field: 'township_id', align:'center'},
    { title: 'Township Code', field: 'township_code' ,align:'center'},
    { title: 'Township Name', field: 'township_name',align:'center'},
    { title: 'State Region',field: 'state_region' ,align:'center',lookup: { 1: 'Magway Region', 2: 'Mandalay Region', 3:'Naypyidaw Unip Territory', 4:' Kayah State', 5:'Shan State', 6:'Yangon Region',
    7:'Kachin' , 8:'Kayin State', 9:'Mon State', 10: 'Chin State', 11:'Chin', 12:'Mon', 13:'Rakhine', 14:'Shan', 15:'Nay Pyi Taw'}},
    { title: 'District',field: 'district' ,align:'center',lookup: { 1: 'Pakokku District', 2: 'Mandalay District', 3:'Dekkhina District', 4:' Loikaw District', 5:'Taunggyi District', 6:'East Yangon District',
    7:'Myitkyina District' , 8:'Hpa-an District', 9:'Mawlamyine  District', 10: 'Hakha District'}},
    { title: 'Remark',field: 'remark' ,align:'center'},
  ]);

  const [data, setData] = useState([
    { township_id: '1', township_code: '90401', township_name: 'Pakokku Township', state_region: 'Magway Region',district:'Pakokku District',remark:'' },
    // { township_id: '2', township_code: '100104', township_name: 'Chanmyatharzi Township', state_region: 'Mandalay Region',district:'Mandalay District',remark:'' },
    // { township_id: '3', township_code: '100604', township_name: 'Pyinmana Township', state_region: 'Naypyidaw Union Territory',district:'Dekkhina District',remark:'' },
    // { township_id: '4', township_code: '09011', township_name: 'Loikaw Township', state_region: 'Kayah State',district:'Loikaw District',remark:'' },
    // { township_id: '5', township_code: '06021', township_name: 'Kalaw Township', state_region: 'Shan State',district:'Taunggyi District',remark:'' },
    // { township_id: '6', township_code: '130209', township_name: 'Bohtataung Township', state_region: 'Yangon Region',district:'East Yangon District',remark:'' },
    // { township_id: '7', township_code: '01011', township_name: 'Myitkyina Township', state_region: 'Kachin State',district:'Myitkyina District',remark:'' },
    // { township_id: '8', township_code: '13018', township_name: 'Pha-an Township', state_region: 'Kayin State',district:'Hpa-an District',remark:'' },
    // { township_id: '9', township_code: '12101', township_name: 'Ye Township', state_region: 'Mon State',district:'Mawlamyine  District',remark:'' },
    // { township_id: '10', township_code: '03011', township_name: 'Hakha Township', state_region: 'Chin State',district:'Hakha District',remark:'' },
  ]);

  return (
    <MaterialTable
      title="Township Preview"
      fontFamily="Poppins"
      columns={columns}
      data={data}

      options={{
        paging:false,
        headerStyle:{fontWeight:'bold', fontSize:16, color:'#000', fontFamily:'Poppins'},
        rowStyle:{fontWeight:'normal', fontSize:16, color:'#505050', fontFamily:'Poppins'},
        actionsCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        filterCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        
      }}
      
      editable={{

        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              setData([...data, newData]);
              resolve();
            }, 1000)
          }),

        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataUpdate = [...data];
              const index = oldData.tableData.id;
              dataUpdate[index] = newData;
              setData([...dataUpdate]);

              resolve();
            }, 1000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataDelete = [...data];
              const index = oldData.tableData.id;
              dataDelete.splice(index, 1);
              setData([...dataDelete]);
              
              resolve()
            }, 1000)
          }),
      }}
    />
  )
}

export default TownShip_Table;